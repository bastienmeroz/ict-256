$(function()
{

    // $("h1").hide();
    // $("h1").fadeIn(3000);
    // $("h1").css("width","0");
    // $("h1").animate({"width:400"})
    // $("h1").show(1000);

    var page_original = true;

    $("img").on( "click", function () {
        if(page_original) {
            $("h1,h2:not('.anecdote')").css("color", "red");
            $("h1,h2:not('.anecdote')").css("border-color", "red");

            $("div#content").css("background-color", "gray");

        }else {
            page_original = false;
        }
    });
});

